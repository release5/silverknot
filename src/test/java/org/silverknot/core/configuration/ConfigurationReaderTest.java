/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.silverknot.core.configuration;

import junit.framework.Assert;
import junit.framework.TestCase;
import org.silverknot.core.authentication.Authentication;
import org.silverknot.core.encryption.EncryptionMethod;
import org.silverknot.core.exception.SilverknotExeption;
import org.silverknot.core.session.Provider;

/**
 * The the parsing of the configuration.
 *
 * @author martin.letendre@release5.com
 */
public class ConfigurationReaderTest extends TestCase {

    public ConfigurationReaderTest(String testName) {
        super(testName);
    }

    /**
     * Test the json configuration.
     *
     * @throws SilverknotExeption
     */
    public void testReadConfiguration() throws SilverknotExeption {
        ConfigurationReader configurationReader = new ConfigurationReader();
        Configuration configuration = configurationReader.getConfiguration("config/silverknot.json");

        SessionConfiguration starWarsBasicConfiguration = configuration.getSessionsConfiguration("star wars basic");
        Assert.assertEquals(Authentication.Simple, starWarsBasicConfiguration.getAuthentication());
        Assert.assertEquals("dc=starwars,dc=com", starWarsBasicConfiguration.getBaseDn());
        Assert.assertEquals("uid=admin,ou=system", starWarsBasicConfiguration.getBindDn());
        Assert.assertEquals("secret", starWarsBasicConfiguration.getBindPassword());
        Assert.assertEquals(EncryptionMethod.NoEncryption, starWarsBasicConfiguration.getEncryptionMethod());
        Assert.assertEquals(10389, starWarsBasicConfiguration.getPort());
        Assert.assertEquals("star wars basic", starWarsBasicConfiguration.getName());
        Assert.assertEquals(Provider.JNDI, starWarsBasicConfiguration.getProvider());


        SessionConfiguration starWarsSecureConfiguration = configuration.getSessionsConfiguration("star wars secure");
        Assert.assertEquals(Authentication.Simple, starWarsSecureConfiguration.getAuthentication());
        Assert.assertEquals("dc=starwars,dc=com", starWarsSecureConfiguration.getBaseDn());
        Assert.assertEquals("uid=admin,ou=system", starWarsSecureConfiguration.getBindDn());
        Assert.assertEquals("secret", starWarsSecureConfiguration.getBindPassword());
        Assert.assertEquals(EncryptionMethod.SSL, starWarsSecureConfiguration.getEncryptionMethod());
        Assert.assertEquals(10636, starWarsSecureConfiguration.getPort());
        Assert.assertEquals("star wars secure", starWarsSecureConfiguration.getName());
        Assert.assertEquals(Provider.JNDI, starWarsSecureConfiguration.getProvider());
    }

    /**
     * Test an invalid configuration.
     *
     * @throws SilverknotExeption
     */
    public void testInvalidReadConfiguration() throws SilverknotExeption {
        ConfigurationReader configurationReader = new ConfigurationReader();
        try {
            Configuration configuration = configurationReader.getConfiguration("config/silverknot-invalid-1.json");
        } catch (SilverknotExeption silverknotExeption) {
            Assert.assertEquals(silverknotExeption.getCode(), "CONF-0003");
        }

        Configuration configuration = configurationReader.getConfiguration("config/silverknot-invalid-2.json");
        SessionConfiguration starWarsBasicConfiguration = configuration.getSessionsConfiguration("star wars basic");
        Assert.assertNull(starWarsBasicConfiguration.getAuthentication());
        Assert.assertEquals("", starWarsBasicConfiguration.getBaseDn());
        Assert.assertNull(starWarsBasicConfiguration.getBindDn());
        Assert.assertNull(starWarsBasicConfiguration.getBindPassword());
        Assert.assertNull(starWarsBasicConfiguration.getEncryptionMethod());
        Assert.assertEquals(0, starWarsBasicConfiguration.getPort());
        Assert.assertNull(starWarsBasicConfiguration.getProvider());
        Assert.assertEquals("star wars basic", starWarsBasicConfiguration.getName());
        
        try {
            starWarsBasicConfiguration.getBindUrl();
            
        } catch (SilverknotExeption silverknotExeption) {
                  Assert.assertEquals(silverknotExeption.getCode(), "CONF-0002");
        }

    }
}
