/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.silverknot.core;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import junit.framework.TestCase;
import org.apache.directory.server.ApacheDsService;
import org.apache.directory.server.core.api.InstanceLayout;

/**
 *
 * @author martin.letendre@release5.com
 */
public class DirectoryTest extends TestCase {

    private static ApacheDsService service;

    public DirectoryTest(String testName) {
        super(testName);
    }
    private static String[] parameters = new String[]{"directory"};

    @Override
    protected void setUp() throws Exception {
        service = new ApacheDsService();

        InstanceLayout instanceLayout = new InstanceLayout("default");

        try {

            service.start(instanceLayout);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testBind() throws NamingException {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldap://localhost:10389/dc=silverknot,dc=com");

        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, "uid=admin,ou=system");
        env.put(Context.SECURITY_CREDENTIALS, "secret");

        DirContext ctx = new InitialDirContext(env);
    }
}
