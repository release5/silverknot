package org.silverknot;


import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.InitialDirContext;
import org.apache.directory.server.ApacheDsService;
import org.apache.directory.server.core.api.InstanceLayout;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author nitram
 */
public class Main {

    private InitialDirContext dirContext;

    /**
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        Main main = new Main();
        main.startDirectoryServer();
        main.bind();
        main.addEntry();
    }

    /**
     *
     */
    private void startDirectoryServer() {

        // Creating ApacheDS service
        ApacheDsService service = new ApacheDsService();
        InstanceLayout instanceLayout = new InstanceLayout("/home/nitram/NetBeansProjects/silverknot/src/test/resources/default");

        try {
            service.start(instanceLayout);
        } catch (Exception e) {

            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     *
     * @throws NamingException
     */
    private void bind() throws NamingException {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldap://localhost:10389/dc=starwars,dc=com");

        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, "uid=admin,ou=system");
        env.put(Context.SECURITY_CREDENTIALS, "secret");

        dirContext = new InitialDirContext(env);
    }

    /**
     *
     * @throws NamingException
     */
    private void addEntry() throws NamingException {

        Attributes attributes = new BasicAttributes(true);
        attributes.put(new BasicAttribute("o", "Jedi"));
        attributes.put(new BasicAttribute("objectclass", "top"));
        attributes.put(new BasicAttribute("objectclass", "organization"));

        dirContext.bind("o=Jedi", dirContext, null);
    }
}
