package org.silverknot.core.annotation;

/**
 * To identify the distinguished name.
 * @author martin.letendre@release5.com
 */
public @interface Dn {
    
}
