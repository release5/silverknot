package org.silverknot.core.annotation;

/**
 * This annotation enables you to identify a java class that your want to map.
 *
 * @author martin.letendre@release5.com
 */
@interface Entry {

    /**
     * If you want to specify a default search root.
     *
     * @return Example: "dc=starwars,dc=com"
     */
    String defaultSearchRoot();
}
