/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.silverknot.core.exception;

/**
 *
 * @author martin.letendre@release5.com
 */
public class SilverknotExeption extends Exception {

    private String code;
    private String message;

    public  SilverknotExeption(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
    
    
}
