/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.silverknot.core.session;

import org.silverknot.core.configuration.Configuration;
import org.silverknot.core.configuration.SessionConfiguration;
import org.silverknot.core.exception.SilverknotExeption;
import static org.silverknot.core.session.Provider.JNDI;
import org.silverknot.core.session.apache.ApacheDirectorySession;
import org.silverknot.core.session.jndi.JNDISession;

/**
 *
 * @author martin.letendre@release5.com
 */
public class SessionFactory {

    private Provider api = Provider.JNDI;
    private Configuration configuration;

    public SessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    public Session openSession(String name) throws SilverknotExeption {

        SessionConfiguration sessionsConfiguration = configuration.getSessionsConfiguration(name);

        switch (api) {
            case JNDI:
                return new JNDISession(sessionsConfiguration);
            case ApacheDirectory:
                return new ApacheDirectorySession();
            default:
                return new JNDISession(sessionsConfiguration);
        }
    }
}
