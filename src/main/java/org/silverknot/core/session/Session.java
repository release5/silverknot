package org.silverknot.core.session;

/**
 *
 * @author martin.letendre@release5.com
 */
public interface Session {

    void add(Object object);

    void search();

    void compare();

    void delete(Object object);

    void update(Object object);

    void rename();

    void close();
}
