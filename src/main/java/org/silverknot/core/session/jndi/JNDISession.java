/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.silverknot.core.session.jndi;

import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;
import org.silverknot.core.authentication.Authentication;
import org.silverknot.core.configuration.SessionConfiguration;
import org.silverknot.core.exception.SilverknotExeption;
import org.silverknot.core.session.Session;

/**
 *
 * @author martin.letendre@release5.com
 */
public class JNDISession implements Session {

    private SessionConfiguration configuration;
    private InitialDirContext dirContext;

    public JNDISession(SessionConfiguration configuration) throws SilverknotExeption {
        this.configuration = configuration;
        bind();
    }

    /* Authenticate and specify LDAP protocol version.*/
    private void bind() throws SilverknotExeption {

        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, configuration.getBindUrl());
        Authentication authentication = configuration.getAuthentication();

        env.put(Context.SECURITY_AUTHENTICATION, authentication.getJndiLabel());

        if (authentication != Authentication.None) {
            env.put(Context.SECURITY_PRINCIPAL, configuration.getBindDn());
            env.put(Context.SECURITY_CREDENTIALS, configuration.getBindPassword());

        }
        
        try {
            dirContext = new InitialDirContext(env);
        } catch (NamingException ex) {
            throw new SilverknotExeption("BIND-0001", "There was a problem binding(connecting) to the directory server:" + ex.getMessage());
        }
    }

    public void add(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void search() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void compare() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void delete(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void update(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void rename() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void close() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
