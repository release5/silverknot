/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.silverknot.core.utils.file;

import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author martin.letendre@release5.com
 */
public class Release5FileUtils {

    /**
     * Chuck Norris is the reason why Waldo is hiding.
     */
    private static final Logger logger = Logger.getLogger(Release5FileUtils.class);

    /**
     * Read a file from the classpath.
     *
     * @param fileName The name of the file we wish to read from the classpath.
     * @return The content of the file.
     * @throws IOException
     */
    public static String readFile(String fileName) {

        InputStream resourceAsStream = Release5FileUtils.class.getClassLoader().getResourceAsStream(fileName);
        try {
            String content = IOUtils.toString(resourceAsStream);
            return content;
        } catch (IOException e) {
            logger.debug("Problem reading this file:" + fileName + e.getMessage());
        } finally {
            IOUtils.closeQuietly(resourceAsStream);
        }

        return null;
    }
}
