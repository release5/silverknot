/*
 * The type of authentication.
 * @author martin.letendre@release5.com
 */
package org.silverknot.core.authentication;

/**
 *
 * @author martin.letendre@release5.com
 */
public enum Authentication {
    
    Simple("simple"), None("none");
    
    private String jndiLabel;

    Authentication(String jndiLabel) {
        this.jndiLabel = jndiLabel;
    }

    /**
     * @return the jndiLabel
     */
    public String getJndiLabel() {
        return jndiLabel;
    }
    
    
}
