/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.silverknot.core.configuration;

import com.google.gson.Gson;
import org.silverknot.core.authentication.Authentication;
import org.silverknot.core.encryption.EncryptionMethod;
import org.silverknot.core.session.Provider;

/**
 *
 * @author martin.letendre@release5.com
 */
public class ConfigurationGenerator {

    public static void main(String[] args) throws Exception {
        ConfigurationGenerator configurationGenerator = new ConfigurationGenerator();
        configurationGenerator.writeConfiguration();
    }

    public void writeConfiguration() {
        Gson gson = new Gson();
        Configuration configuration = new Configuration();
        SessionConfiguration sessionConfiguration = new SessionConfiguration();
        sessionConfiguration.setAuthentication(Authentication.Simple);
        sessionConfiguration.setBaseDn("dc=starwars,dc=com");
        sessionConfiguration.setBindDn("uid=admin,ou=system");
        sessionConfiguration.setBindPassword("secret");
        sessionConfiguration.setEncryptionMethod(EncryptionMethod.NoEncryption);
        sessionConfiguration.setName("star wars basic");
        sessionConfiguration.setPort(10389);
        sessionConfiguration.setProvider(Provider.JNDI);
        configuration.addSessionsConfiguration(sessionConfiguration);
        String json = gson.toJson(configuration);
        System.out.println("************************");
        System.out.print(json);
        System.out.println("************************");

    }
}
