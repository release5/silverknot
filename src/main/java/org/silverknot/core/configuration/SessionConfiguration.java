/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.silverknot.core.configuration;

import org.apache.commons.lang.StringUtils;
import org.silverknot.core.authentication.Authentication;
import org.silverknot.core.encryption.EncryptionMethod;
import org.silverknot.core.exception.SilverknotExeption;
import org.silverknot.core.session.Provider;

/**
 * The session configuration.
 *
 * @author martin.letendre@release5.com
 */
public class SessionConfiguration {

    /**
     * The name of the connection. The name must be unique.
     */
    private String name;
    /**
     * The host name or IP address of the LDAP server.
     */
    private String hostname;
    /**
     * The port of the LDAP server. The default port for non-encyrpted
     * connections is 389.
     */
    private int port;
    /**
     * The user name to bind to the Directory.
     */
    private String bindDn;
    /**
     * The password to bind to the Directory.
     */
    private String bindPassword;
    /**
     * The Entry unique identifier: its Distinguished Name (DN). This consists
     * of its Relative Distinguished Name (RDN), constructed from some
     * attribute(s) in the entry, followed by the parent entry's DN. </br>
     * Example: o=starwars,o=movie
     */
    private String baseDn = "";
    /**
     * The library used to bind to the directory.
     */
    private Provider provider;
    /**
     * The type of authentication.
     */
    private Authentication authentication;
    /**
     * The encryption to use. Possible values are NoEncryption or SSL. Default
     * value is NoEncryption.
     */
    private EncryptionMethod encryptionMethod;

    /**
     * The port of the LDAP server.
     *
     * @return The default port for non-encyrpted connections is 389.
     */
    public int getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * @return the bindDn
     */
    public String getBindDn() {
        return bindDn;
    }

    /**
     * @param bindDn the bindDn to set
     */
    public void setBindDn(String bindDn) {
        this.bindDn = bindDn;
    }

    /**
     * @return the bindPassword
     */
    public String getBindPassword() {
        return bindPassword;
    }

    /**
     * @param bindPassword the bindPassword to set
     */
    public void setBindPassword(String bindPassword) {
        this.bindPassword = bindPassword;
    }

    /**
     * @return the baseDn
     */
    public String getBaseDn() {
        return baseDn;
    }

    /**
     * @param baseDn the baseDn to set
     */
    public void setBaseDn(String baseDn) {
        this.baseDn = baseDn;
    }

    /**
     * @return the provider
     */
    public Provider getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    /**
     * @return the authentication
     */
    public Authentication getAuthentication() {
        return authentication;
    }

    /**
     * @param authentication the authentication to set
     */
    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }

    /**
     * @return the encryptionMethod
     */
    public EncryptionMethod getEncryptionMethod() {
        return encryptionMethod;
    }

    /**
     * @param encryptionMethod the encryptionMethod to set
     */
    public void setEncryptionMethod(EncryptionMethod encryptionMethod) {
        this.encryptionMethod = encryptionMethod;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * The bind url. <br/>
     *
     * @return Example: ldap://localhost:10389/dc=starwars,dc=com
     * @throws SilverknotExeption
     */
    public String getBindUrl() throws SilverknotExeption {

        String url = "";

        if (StringUtils.isEmpty(hostname)) {
            throw new SilverknotExeption("CONF-0002", "You must provide a hostname for connection: " + name);
        }

        switch (encryptionMethod) {

            case NoEncryption:
                url = "ldap://";
                break;
            case SSL:
                url = "ldaps://";
                break;
            default:
                throw new SilverknotExeption("CONF-0004", "Unsupported encryption method: " + encryptionMethod);
        }
        
        url = url + hostname + ":" + port + "/" + baseDn;

        return url;

    }
}
