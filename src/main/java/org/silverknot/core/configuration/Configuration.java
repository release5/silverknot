package org.silverknot.core.configuration;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import org.silverknot.core.exception.SilverknotExeption;

/**
 * The configuration of the connections.
 *
 * @author martin.letendre@release5.com
 */
public class Configuration {

    /**
     * A
     */
    private List<SessionConfiguration> sessionConfigurations;

    /**
     * @return the sessionsConfiguration
     */
    public SessionConfiguration getSessionsConfiguration(String name) throws SilverknotExeption {
        for (SessionConfiguration configuration : sessionConfigurations) {

            if (configuration.getName() != null && name != null && name.equals(configuration.getName())) {
                return configuration;
            }
        }

        throw new SilverknotExeption("CONF-001", "The session configuration with name " + name + " was not found");

    }

    /**
     * @param sessionsConfiguration the sessionsConfiguration to set
     */
    public void setSessionsConfiguration(List<SessionConfiguration> sessionConfigurations) {
        this.sessionConfigurations = sessionConfigurations;
    }

    /**
     *
     * @param sessionConfiguration
     */
    public void addSessionsConfiguration(SessionConfiguration sessionConfiguration) {

        if (sessionConfigurations == null) {
            sessionConfigurations = new ArrayList<SessionConfiguration>();
        }

        sessionConfigurations.add(sessionConfiguration);
    }

    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
