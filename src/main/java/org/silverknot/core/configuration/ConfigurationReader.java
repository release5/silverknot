/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.silverknot.core.configuration;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.silverknot.core.exception.SilverknotExeption;
import org.silverknot.core.utils.file.Release5FileUtils;

/**
 * Enable you to read a configuration.
 *
 * @author martin.letendre@release5.com
 */
public class ConfigurationReader {

    /**
     * Read the configuration of the connections from a file in the classpath.
     *
     * @param fileName The name of the file in the classpath.
     * @return The configuration of the connections.
     */
    public Configuration getConfiguration(String fileName) throws SilverknotExeption {
        String expression = Release5FileUtils.readFile(fileName);
        return getConfigurationExpression(expression);
    }

    /**
     *
     * @param jsonExpression A json representation of the configuration objects.
     * @return A configuration object.
     */
    public Configuration getConfigurationExpression(String jsonExpression) throws SilverknotExeption {
        Gson gson = new Gson();

        try {
            return gson.fromJson(jsonExpression, Configuration.class);
        } catch ( JsonSyntaxException jsonSyntaxException) {
            throw new SilverknotExeption("CONF-0003", "The json format is invalid" + jsonSyntaxException.getMessage());
        }
    }
}
